package com.example.timesheet;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ProjectActivity extends AppCompatActivity {
    List<String> Items;
    List<String> ListItems;
    ListView listview;
    EditText ProjectName;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        listview = findViewById(R.id.ProjectList);
        ProjectName = findViewById(R.id.ProjectName);
        Items = new ArrayList<>();
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        deleteItemListener();
        prepareData();
        refreshDisplay();
    }

    private void deleteItemListener() {
        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ProjectActivity.this);
                builder.setTitle("Delete Project")
                        .setMessage("Are you delete " + Items.get(position))
                        .setCancelable(false)
                        .setNegativeButton("cancel",null)
                        .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Items.remove(position);
                                adapter.notifyDataSetChanged();
                                Toast.makeText(ProjectActivity.this, "Delete is complete", Toast.LENGTH_LONG).show();
                                CreateInternalFile(Items.toString().getBytes());
                            }
                        });
                builder.show();
                return false;
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == android.R.id.home){
            startActivity(new Intent(ProjectActivity.this, MainActivity.class));
        }
        return super.onOptionsItemSelected(item);

    }

    public void prepareData() {
        ReadInternalFile();
        if(ListItems == null){
            Items.add("Default Project");
            String list = Items.toString();
            CreateInternalFile(list.getBytes());
            Collections.sort(Items);
            adapter.notifyDataSetChanged();
        }

    }

    private void ReadInternalFile() {
        try {
            FileInputStream fis = openFileInput("Projects");
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String allList = reader.readLine();
            ListItems = Arrays.asList(allList.substring(1, allList.length() - 1).split(","));
            for (String Item : ListItems) {
                Items.add(Item.trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void refreshDisplay() {
        Collections.sort(Items);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Items);
        listview.setAdapter(adapter);

    }

    public void Add(View v) {
        String pn = ProjectName.getText().toString().trim();
        if (pn.isEmpty()) {
            ProjectName.setError("Project Name is Required");
        } else {
            Items.add(pn);
            String list = Items.toString();
            CreateInternalFile(list.getBytes());
            Collections.sort(Items);
            adapter.notifyDataSetChanged();
            ProjectName.setText("");
        }
    }

    private void CreateInternalFile(byte[] bytes) {
        try {
            FileOutputStream fos = openFileOutput("Projects", MODE_PRIVATE);
            fos.write(bytes);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
