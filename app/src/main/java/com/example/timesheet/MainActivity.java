package com.example.timesheet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Spinner spinner;
    String[] ListItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = findViewById(R.id.spinnerProjectList);
        ReadInternalFile();
        if(ListItems == null){
            startActivity(new Intent(MainActivity.this, ProjectActivity.class));
        }
//        Toast.makeText(this, Arrays.toString(ListItems), Toast.LENGTH_SHORT).show();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, ListItems);
        spinner.setAdapter(adapter);
    }

    private void ReadInternalFile() {


        try {
            FileInputStream fis = openFileInput("Projects");
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String allList = reader.readLine();
            ListItems = (String[]) Arrays.asList(allList.substring(1, allList.length() - 1).split(",")).toArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Project").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(MainActivity.this, ProjectActivity.class));
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


}
